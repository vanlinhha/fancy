package com.example.fancy.domains.unit;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "units")
public class Unit {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

    @Column(name = "name")
    public String name;

    @Column(name = "description")
    public String description;

//    @Column(name = "created")
//    public String created;
//
//    @Column(name = "modified")
//    public String modified;

    @Column(name = "path")
    public String path;

    @Column(name = "order")
    public Integer order;

    @Column(name = "level")
    public Integer level;

    @Column(name = "status")
    public Integer status;






    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
