package com.example.fancy.responses;

import lombok.Data;

import java.util.List;

@Data
public class DefaultResponse<T> {
  private boolean success;
  private String message;
  private List<T> data;

  public void success() {
    this.setMessage("Thành công");
    this.setSuccess(true);
  }

  public static <T> DefaultResponse<T> success(List<T> data) {
    DefaultResponse<T> response = new DefaultResponse<>();
    response.success();
    response.setData(data);
    return response;
  }

  public static <T> DefaultResponse<T> error(String message) {
    DefaultResponse<T> response = new DefaultResponse<>();
    response.setSuccess(false);
    response.setMessage(message);
    return response;
  }

  public static <T> DefaultResponse<T> error(String message, List<T> data) {
    DefaultResponse<T> response = new DefaultResponse<>();
    response.setSuccess(false);
    response.setMessage(message);
    response.setData(data);
    return response;
  }

  public static <T> DefaultResponse<T> notFound() {
    DefaultResponse<T> dto = new DefaultResponse<>();
    dto.setSuccess(false);
    dto.setMessage("Không tìm thấy event");
    return dto;
  }

}
