package com.example.fancy.responses.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.util.StringUtils;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResponse<T> {
    private boolean success;
    private T data;
    private String message;
    private int code;
    @JsonProperty("rid")
    private String requestId;

    @JsonProperty("rid")
    public String getRequestId() {
        if (StringUtils.hasLength(requestId)) return requestId;
        return null;
    }
}
