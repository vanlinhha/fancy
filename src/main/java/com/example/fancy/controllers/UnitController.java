package com.example.fancy.controllers;


import com.example.fancy.demo.UnitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/units")
public class UnitController {

    @Autowired
    UnitRepository unitRepository;

    @GetMapping("")
    public ResponseEntity<?> index() {
        return ResponseEntity.ok(unitRepository.findAll());
    }


}
