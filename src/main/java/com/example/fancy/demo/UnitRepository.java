package com.example.fancy.demo;

import com.example.fancy.domains.unit.Unit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnitRepository extends JpaRepository<Unit, Long> {

    public Unit findById(long id);

}
